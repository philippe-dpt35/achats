Adaptation des jeux "Achats en euros" et "La monnaie" de la compilation d'applications Clicmenu.

Il faut placer pièces et billets sur le comptoire de façon à régler le montant du prix d'un objet.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/achats/index.html)
